#ifndef SI7021_H
#define SI7021_H

#define SI7021_DEFAULT_ADDRESS           0x40
#define SI7021_DEFAULT_SETTINGS          0x3a

// I2C commands
#define SI7021_CMD_MEASRH_HOLD           0xE5
#define SI7021_CMD_MEASRH_NOHOLD         0xF5
#define SI7021_CMD_MEASTEMP_HOLD         0xE3
#define SI7021_CMD_MEASTEMP_NOHOLD       0xF3
#define SI7021_CMD_READPREVTEMP          0xE0
#define SI7021_CMD_RESET                 0xFE
#define SI7021_CMD_WRITERHT_REG          0xE6
#define SI7021_CMD_READRHT_REG           0xE7
#define SI7021_CMD_WRITEHEATER_REG       0x51
#define SI7021_CMD_READHEATER_REG        0x11
// those 2 byte commands are endianess-swapped to be sent bytewise
#define SI7021_CMD_ID1                   0x0FFA
#define SI7021_CMD_ID2                   0xC9FC
#define SI7021_CMD_FIRMVERS              0xB884

// configuration
#define SI7021_CFGBIT_RES0     0
#define SI7021_CFGBIT_HTRE     2
#define SI7021_CFGBIT_VDDS     6
#define SI7021_CFGBIT_RES1     7

#define SI7021_HEATER_MASK     0x0f

extern void sensor_cmd_cycle(void);

extern float Temperature;
extern float Humidity;

#endif /* SI7021_H */