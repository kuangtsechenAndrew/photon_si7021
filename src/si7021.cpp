/*
 * Reference: https://github.com/particleflux/Si7021
 */

#include "application.h"
#include "si7021.h"

void sensor_cmd_cycle(void);

void send_measure_temp_cmd()
{
    Wire.beginTransmission(SI7021_DEFAULT_ADDRESS);
    Wire.write(SI7021_CMD_READPREVTEMP);
    Wire.endTransmission();

    return ;
}

void send_measure_humd_cmd()
{
    Wire.beginTransmission(SI7021_DEFAULT_ADDRESS);
    Wire.write(SI7021_CMD_MEASRH_NOHOLD);
    Wire.endTransmission();

    return ;
}

float send_read_tmp_cmd()
{
    float TemperatureLocal;
    uint8_t buffer[2] = {0};

    Wire.requestFrom(SI7021_DEFAULT_ADDRESS, 2);

    for(int i = 0; i < 2; ++i) 
    {
        buffer[i] = Wire.read();
    }

     TemperatureLocal = ((float) (((uint16_t) buffer[0] << 8) | buffer[1]) * 175.72) / 65536 - 46.85;

    return TemperatureLocal;
}

float send_read_humidity_cmd()
{
    float humidity;
    uint8_t buffer[2] = {0};

    Wire.requestFrom(SI7021_DEFAULT_ADDRESS, 3);

    for(int i = 0; i < 2; ++i) 
    {
        buffer[i] = Wire.read();
    }

    humidity = ((float) (((uint16_t) buffer[0] << 8) | buffer[1]) * 125) / 65536 - 6;
    
    // clamp values to 0-100% range
    // it is in some cases possible that the values are outside of this range, according to spec
    if (humidity < 0) 
    {
        humidity = 0.0;
    } 
    else if (humidity > 100.0) 
    {
        humidity = 100.0;
    }
    else
    {
        /* Do nothing here. */
    }
    

    return humidity;
}

void sensor_cmd_cycle()
{
    static uint8_t current_cycle = 0;

    switch (current_cycle)
    {
    case 0:
        send_measure_humd_cmd();
        break;
    
    case 1:
        Humidity =  send_read_humidity_cmd();
        break;

    case 2:
        send_measure_temp_cmd();
        break;

    case 3:
        Temperature = send_read_tmp_cmd();
        break;

    default:
        break;
    }

    current_cycle ++;
    if(current_cycle > 3)
    {
        current_cycle = 0;
    }

    return ; 
}