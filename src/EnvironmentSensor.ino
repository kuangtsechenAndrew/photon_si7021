/*
 * Project Si_7021
 * Description: Using I2C to read temperature and humidity sensor.
 * Author: Andrew
 * Date: 2020/11/17
 */
#include "si7021.h"
/* Prevent system auto-connect to the network.*/
SYSTEM_MODE(SEMI_AUTOMATIC);

void one_hund_task();
void one_second_task();

Timer timer1(100, one_hund_task);
Timer timer2(1000, one_second_task);

float Temperature = 0;
float Humidity = 0;

void setup() 
{
    /* Turn off wifi first */
    WiFi.off();

    /* Setup serial communication */
    Serial.begin(9600);

    /* Setup I2C interface*/
    Wire.begin();

    /* Start the timer */
    timer1.start();
    timer2.start();

}


void loop() {
  

}

void one_hund_task()
{
    sensor_cmd_cycle();
}

void one_second_task()
{    
    Serial.printf("Temperature: %.2f degree C \n",Temperature);
    Serial.printf("Humidity: %.2f %% \n",Humidity);  
    Serial.println("");  
}
