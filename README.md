# Environment setup
MCU: Particle Photon
Sensor module: Sparkfun Si7021

## I2C Pin Connection
SCL(Si7021) => D1(Particle)

SDA(Si7021) => D0(Particle)

Also, remember to connect 3.3V and GND

# Scope
This project aims to provide a simple example of using Particle photon and Si7021. 

The main communication interface between Photon and Si7021 is I2C which is a common interface.

Originally, I was learning it from https://github.com/particleflux/Si7021

It provides a complete protocol and features of accessing Si7021 which included CRC and heating function.

However, an I2C nooby like me could take a while to understand the full function.

That is why I decide to simplify it to minimum code length and function.

# LICENSE
Copyright 2020 Andrew C

Licensed under the MIT license